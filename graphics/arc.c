#include "arc.h"

static void graphics_slice_impl(GContext *ctx, GPoint center, uint16_t radius, uint16_t start, uint16_t end, void (*draw_func)(GContext *, GPath *));
static void graphics_arc_impl(GContext *ctx, GPoint center, uint16_t radius, uint16_t thickness, uint16_t start, uint16_t end, void (*draw_func)(GContext *, GPath *));

void graphics_draw_slice(GContext *ctx, GPoint center, uint16_t radius, uint16_t start, uint16_t end) {
  graphics_slice_impl(ctx, center, radius, start, end, &gpath_draw_outline);
}

void graphics_fill_slice(GContext *ctx, GPoint center, uint16_t radius, uint16_t start, uint16_t end) {
  graphics_slice_impl(ctx, center, radius, start, end, &gpath_draw_filled);
}

void graphics_draw_arc(GContext *ctx, GPoint center, uint16_t radius, uint16_t thickness, uint16_t start, uint16_t end) {
  graphics_arc_impl(ctx, center, radius, thickness, start, end, &gpath_draw_outline);
}

void graphics_fill_arc(GContext *ctx, GPoint center, uint16_t radius, uint16_t thickness, uint16_t start, uint16_t end) {
  graphics_arc_impl(ctx, center, radius, thickness, start, end, &gpath_draw_filled);
}

void graphics_arc_impl(GContext *ctx, GPoint center, uint16_t radius, uint16_t thickness, uint16_t start, uint16_t end, void (*draw_func)(GContext *, GPath *)) {
  int degrees = (end - start);

  GPoint arc_points[degrees * 2];
  for (int16_t i = 0; i < degrees; ++i) {
    int32_t angle = TRIG_MAX_ANGLE * (i + start) / 360;

    arc_points[i] = (GPoint) {
      .x = (sin_lookup(angle) * (radius + 1) / TRIG_MAX_RATIO) + center.x,
      .y = (-cos_lookup(angle) * (radius + 1) / TRIG_MAX_RATIO) + center.y
    };
  }

  for (int16_t i = degrees - 1; i >= 0; --i) {
    int32_t angle = TRIG_MAX_ANGLE * (end - i) / 360;

    arc_points[degrees + i] = (GPoint) {
      .x = (sin_lookup(angle) * (radius - thickness + 1) / TRIG_MAX_RATIO) + center.x,
      .y = (-cos_lookup(angle) * (radius - thickness + 1) / TRIG_MAX_RATIO) + center.y
    };
  }

  GPathInfo arc_info = {degrees * 2, arc_points};
  GPath *arc = gpath_create(&arc_info);
  draw_func(ctx, arc);
  gpath_destroy(arc);
}

static void graphics_slice_impl(GContext *ctx, GPoint center, uint16_t radius, uint16_t start, uint16_t end,
    void (*draw_func)(GContext *, GPath *)) {
  int degrees = (end - start);

  GPoint arc_points[degrees + 1];
  arc_points[0] = center;

  for (uint16_t i = 0; i < degrees; ++i) {
    int32_t angle = TRIG_MAX_ANGLE * (i + start) / 360;

    arc_points[i + 1] = (GPoint) {
      .x = (sin_lookup(angle) * (radius + 1) / TRIG_MAX_RATIO) + center.x,
      .y = (-cos_lookup(angle) * (radius + 1) / TRIG_MAX_RATIO) + center.y
    };
  }

  GPathInfo arc_info = {degrees + 1, arc_points};
  GPath *arc = gpath_create(&arc_info);
  draw_func(ctx, arc);
  gpath_destroy(arc);
}