#include <pebble.h>

void graphics_draw_arc(GContext *ctx, GPoint center, uint16_t radius, uint16_t thickness, uint16_t start, uint16_t end);
void graphics_fill_arc(GContext *ctx, GPoint center, uint16_t radius, uint16_t thickness, uint16_t start, uint16_t end);

void graphics_draw_slice(GContext *ctx, GPoint center, uint16_t radius, uint16_t start, uint16_t end);
void graphics_fill_slice(GContext *ctx, GPoint center, uint16_t radius, uint16_t start, uint16_t end);