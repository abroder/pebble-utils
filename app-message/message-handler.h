#include <pebble.h>

typedef void (*MessageCallback)(Tuple *data);

void message_handler_create();
void message_handler_destroy();

void message_handler_subscribe(uint32_t key, MessageCallback callback);
void message_handler_publish_uint16(uint32_t key, uint16_t data);
void message_handler_publish_uint32(uint32_t key, uint32_t data);