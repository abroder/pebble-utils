#include <pebble.h>

typedef void (*MessageCallback)(Tuple *);

void message_handler_create();
void message_handler_destroy();

void message_handler_subscribe(uint32_t, MessageCallback);
void message_handler_publish_uint16(uint32_t, uint16_t);
void message_handler_publish_uint32(uint32_t, uint32_t);