#include "message-handler.h"

static void inbox_received_callback(DictionaryIterator *iterator, void *context);
static void inbox_dropped_callback(AppMessageResult reason, void *context);
static void outbox_sent_callback(DictionaryIterator *iterator, void *context);
static void outbox_failed_callback(DictionaryIterator *iterator, AppMessageResult reason, void *context);

struct MessageHandler {
  uint32_t key;
  MessageCallback callback;
  struct MessageHandler *next;
};
typedef struct MessageHandler MessageHandler;

static MessageHandler *s_head;

void message_handler_create() {
  app_message_register_inbox_received(inbox_received_callback);
  app_message_register_inbox_dropped(inbox_dropped_callback);
  app_message_register_outbox_failed(outbox_failed_callback);
  app_message_register_outbox_sent(outbox_sent_callback);
  
  app_message_open(app_message_inbox_size_maximum(), app_message_outbox_size_maximum());
}

void message_handler_destroy() {
  while (s_head != NULL) {
    MessageHandler *curr = s_head;
    s_head = s_head->next;
    free(curr);
  }
}

void message_handler_subscribe(uint32_t key, MessageCallback callback) {
  MessageHandler *handler = malloc(sizeof(MessageHandler));
  handler->next = s_head;
  handler->key = key;
  handler->callback = callback;

  s_head = handler;
}

void message_handler_publish_uint16(uint32_t key, uint16_t data) {
  DictionaryIterator *iterator;
  app_message_outbox_begin(&iterator);
  dict_write_uint16(iterator, key, data);
  app_message_outbox_send();
}

void message_handler_publish_uint32(uint32_t key, uint32_t data) {
  DictionaryIterator *iterator;
  app_message_outbox_begin(&iterator);
  dict_write_uint32(iterator, key, data);
  app_message_outbox_send();
}

static void inbox_received_callback(DictionaryIterator *iterator, void *context) {
  Tuple *t = dict_read_first(iterator);

  while (t != NULL) {
    uint32_t key = t->key;
    MessageHandler *curr = s_head;
    while (curr != NULL) {
      if (curr->key == key) {
        curr->callback(t);
      }
      curr = curr->next;
    }

    t = dict_read_next(iterator);
  }
}

static void inbox_dropped_callback(AppMessageResult reason, void *context) {
}

static void outbox_failed_callback(DictionaryIterator *iterator, AppMessageResult reason, void *context) {
}

static void outbox_sent_callback(DictionaryIterator *iterator, void *context) {
}
