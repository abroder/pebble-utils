#include <pebble.h>

char *read_length_encoded_string(const char *, char **);
uint32_t read_uint(const char *, char **);