#include <pebble.h>

char *read_length_encoded_string(const char *str, char **endptr);
uint32_t read_uint(const char *str, char **endptr);