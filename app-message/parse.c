#include "parse.h"

char *read_length_encoded_string(const char *str, char **endptr) {
  uint8_t length = str[0];
  if (length == 0) {
    *endptr = (char *)(str + 1);
    return NULL;
  }

  char *result = malloc(length + 1);
  memcpy(result, &(str[1]), length);
  result[length] = '\0';
  if (endptr != NULL) {
    *endptr = (char *)(str + length + 1);
  }
  return result;
}

uint32_t read_uint(const char *str, char **endptr) {
  char *value_str = read_length_encoded_string(str, endptr);
  if (value_str == NULL) return 0;
  uint32_t value = atoi(value_str);
  free(value_str);

  return value;
}