#include "strings.h"

char *strdup(char *str) {
  size_t len = strlen(str);
  char *result = malloc(len + 1);
  memcpy(result, str, len);
  result[len] = '\0';
  return result;
}